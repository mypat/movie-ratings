package org.example.movie.app.entity;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class StringListConverterTest {
    StringListConverter subject = new StringListConverter();

    @Test
    public void shouldConvertNullString() {
        assertThat(subject.convertToEntityAttribute(null)).isEqualTo(Collections.emptyList());
    }

    @Test
    public void shouldConvertEmptyString() {
        assertThat(subject.convertToEntityAttribute("")).isEqualTo(Collections.emptyList());
    }

    @Test
    public void shouldConvertExampleString() {
        assertThat(subject.convertToEntityAttribute("abc;bcd")).isEqualTo(Arrays.asList("abc", "bcd"));
    }

    @Test
    public void shouldConvertNullArray() {
        assertThat(subject.convertToDatabaseColumn(null)).isEqualTo("");
    }

    @Test
    public void shouldConvertEmptyArray() {
        assertThat(subject.convertToDatabaseColumn(Collections.emptyList())).isEqualTo("");
    }

    @Test
    public void shouldConvertExampleArray() {
        assertThat(subject.convertToDatabaseColumn(Arrays.asList("abc", "bcd"))).isEqualTo("abc;bcd");
    }

}