package org.example.movie.app.entity;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Converter
public class StringListConverter implements AttributeConverter<List<String>, String> {
    private static final String SPLIT_BY = ";";

    @Override
    public String convertToDatabaseColumn(List<String> stringList) {
        return stringList == null || stringList.isEmpty() ? "" : String.join(SPLIT_BY, stringList);
    }

    @Override
    public List<String> convertToEntityAttribute(String string) {
        return StringUtils.isEmpty(string) ? Collections.emptyList() : Arrays.asList(string.split(SPLIT_BY));
    }
}