package org.example.movie.film.entity;

import lombok.*;
import org.example.movie.app.entity.StringListConverter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "t_film")
@SequenceGenerator(name = FilmBE.SEQ_NAME, sequenceName = FilmBE.SEQ_NAME)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class FilmBE {
    public static final String SEQ_NAME = "sq_film";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = FilmBE.SEQ_NAME)
    private Long id;
    private String name;
    @Convert(converter = StringListConverter.class)
    private List<String> directors;
    @Convert(converter = StringListConverter.class)
    private List<String> genres;
}
