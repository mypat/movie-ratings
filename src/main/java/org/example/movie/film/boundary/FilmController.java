package org.example.movie.film.boundary;

import org.example.movie.film.entity.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FilmController {
    @Autowired
    private FilmRepository filmRepo;

    @GetMapping(path = "films", produces = "application/json")
    public List<FilmTO> list() {
        return filmRepo.findAll().stream().map(FilmTO::from).collect(Collectors.toList());
    }
}
