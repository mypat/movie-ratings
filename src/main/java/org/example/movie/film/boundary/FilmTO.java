package org.example.movie.film.boundary;

import lombok.*;
import org.example.movie.film.entity.FilmBE;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class FilmTO {
    private String name;
    private List<String> directors;
    private List<String> genres;
    private float rating; // TODO calculate
    private int ratedByCount;

    public static FilmTO from(FilmBE entity) {
        return FilmTO.builder()
                .directors(entity.getDirectors())
                .genres(entity.getGenres())
                .name(entity.getName())
                .build();
    }
}
